package base;

import java.awt.image.BufferedImage;
import java.util.Queue;

public class Vehicle extends Unit {

	BufferedImage img_drv;
	public boolean driving = false;
	private boolean drvimgchange;
	private int imgchangecounter;
	
	public Vehicle(BufferedImage img, BufferedImage img2, int x, int y, Game game, int s, int def, int dmg) {
		super(img, x, y, game, s, def, dmg);
		img_drv = img2;
		
	}

	public BufferedImage getImage() {
		if(drvimgchange) return this.img_drv;
		else return this.image;
	}
	
	@Override
	void tick() {
		super.tick();
		
	}
	
	void quicktick() {
		super.quicktick();
		if(driving && imgchangecounter > 8) {
			drvimgchange = !drvimgchange;
			imgchangecounter = 0;
		}
		imgchangecounter++;
	}
	
	@Override
	void clicked() {
		super.clicked();
		driving = true;
		System.out.println("Unit "+this+" started driving");
		
	}
	
	void move(int x, int y, Queue<Square> r, Square s) {
		driving = true;
		System.out.println("Unit "+this+" started driving");
		super.move(x, y, r, s);
	}
	
	void endMove() {
		super.endMove();
		System.out.println("Unit "+this+" stopped driving");
		driving = false;
	}
}
