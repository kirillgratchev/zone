package base;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Queue;

public class Unit extends Character{

	boolean moving = false, attacking = false, squarelocked = false, pivot;
	int sX, sY;
	int offX, offY;
	Queue<Square> route = new LinkedList<>();
	Game game;
	int lockedX, lockedY;
	Square toAttack;
	
	int speed, health, defense, damage;
	
	public Unit(BufferedImage img, int x, int y, Game game, int speed, int defense, int damage) {
		super(img, x, y);
		this.game = game;
		
		sX = x;
		sY = y;
		
		this.speed = speed;
		this.health = 100;
		this.defense = defense;
		this.damage = damage;
	}

	void tick() {
		if(moving) {
			
		}
	}
	
	void getDamaged(int dmg) {
		double factor = defense;
		factor /= 100;
		factor = 1 - factor;
		dmg = (int)(dmg * factor);
		health -= dmg;
		if(health <= 0) {
			game.terminateUnit(this);
			System.out.println("Unit "+this+" damaged for "+dmg+" and terminated");
		} else
		System.out.println("Unit "+this+" damaged for "+dmg+" and now at "+health+" health");
	}
	
	int getHealth() {
		if (health >= 95) {
			return 10;
		} else if (health >= 85) {
			return 9;
		} else if (health >= 75) {
			return 8;
		} else if (health >= 65) {
			return 7;
		} else if (health >= 55) {
			return 6;
		} else if (health >= 45) {
			return 5;
		} else if (health >= 35) {
			return 4;
		} else if (health >= 25) {
			return 3;
		} else if (health >= 15) {
			return 2;
		} else {
			return 1;
		}
	}

	void quicktick() {
		if(moving) {
			graphicalMove();
		} else if(attacking) {
			graphicalAttack();
		}
	}
	
	void graphicalAttack() {
		if(toAttack.x > x) {
			pivot = false;
		}else if(toAttack.x < x) {
			pivot = true;
		}
		toAttack.unitOnSquare.getDamaged(damage);
		endAttack();
	}
	
	void graphicalMove(){
		if(squarelocked == false && !route.isEmpty()) {
			
			lockedX = route.peek().x;
			lockedY = route.peek().y;
			System.out.println("Unit "+this+" locked onto "+lockedX+"-"+lockedY);
			squarelocked = true;
			
			if(lockedX < sX) {
				pivot = true;
			} else if(lockedX > sX) {
				pivot = false;
			}
			
		} else if(squarelocked) {
			if(lockedX != sX) {
				if(lockedX < sX) {
					offX -= 1;
					reachSquare();
				} else if(lockedX > sX) {
					offX += 1;
					reachSquare();
				}
			} else if(lockedY != sY) {
				if(lockedY < sY) {
					offY -= 1;
					reachSquare();
				} else if(lockedY > sY) {
					offY += 1;
					reachSquare();
				}
			} //else endMove();//HMMMMM
		} else endMove();
	}
	
	void clicked() {
		super.clicked();
	}

	void endMove() {
		super.endAction();
		System.out.println("Unit "+this+" ending Movement");
		route.clear(); //HMMMMMM
		game.squares[x][y].gettingMovedTo = false;
		moving = false;
		sX = x;
		sY = y;
		if(toAttack != null) {
			attacking = true;
		}
	}
	
	void endAttack() {
		attacking = false;
		System.out.println("Unit "+this+" ending Attack");
	}

	void reachSquare() {
		
		if (offX <= -15 || offX >= 15 || offY <= -15 || offY >= 15) {
			if (route.isEmpty() == false) {
				
				sX = route.peek().x;
				sY = route.peek().y;
				
				System.out.println("Unit "+this+" went through "+sX+"-"+sY);
				
				route.poll();
				if (route.isEmpty()) {
					endMove();
				}
			} else {
				endMove();
			}
			
			offX = 0;
			offY = 0;
			squarelocked = false;
		}
	}

	void move(int x, int y, Queue<Square> r, Square toAttack) {
		moving = true;
		game.squares[x][y].gettingMovedTo = true;
		route.addAll(r);
		this.toAttack = toAttack;
		
		if(this.x == x && this.y == y) { //HMMMM
			endMove();
		}
		game.moveUnit(this, x, y);
		
	}
}
