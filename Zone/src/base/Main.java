package base;
import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.Timer;

public class Main{

	public static void main(String[] args) {
		
		World world = new World(30, 20);
		Game game = new Game(world);
		
		JFrame window = new JFrame();
		
		window.setBounds(0, 0, 1400, 500);
		window.setResizable(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		
		
		Screen screen = new Screen(game);
		screen.setVisible(true);
		screen.setLayout(null);
		screen.setBackground(Color.DARK_GRAY);
		
		Timer timer = new Timer(15, screen);
		timer.start();
		
		window.addMouseListener(screen);
		window.addWindowStateListener(screen);
		window.addComponentListener(screen);
		window.addKeyListener(screen);
		window.addMouseWheelListener(screen);
		window.add(screen);
		
		window.validate();
		window.repaint();
		

	}


}
