package base;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.swing.JPanel;

public class Screen extends JPanel implements KeyListener, ActionListener, MouseListener, MouseWheelListener, ComponentListener, WindowStateListener{

	Game game;

	boolean keyLeft, keyRight, keyUp, keyDown;
	boolean lMouseDown, lClickPerformed;
	boolean rMouseDown, rClickPerformed;
	int mouseX, mouseY;

	int winFSOffX = 8, winFSOffY = 32;
	boolean winIsMax, winWasMax;
	int winWidth, winHeight;
	int drawOffsetX, drawOffsetY;
	int scale = 48;
	double c = 1.5;

	boolean selectorImgTick;
	int tickCounter;

	boolean inAction;
	Unit perform;
	int performMovesLeft;
	boolean performMoved, performAttacked;

	Queue<Square> drawnroad = new LinkedList<Square>();
	List<Square> goArea = new ArrayList<Square>();
	List<Square> attackable = new ArrayList<Square>();

	int currentSquareX, currentSquareY, lastSquareX, lastSquareY;
	int drawnArrowX, drawnArrowY;

	Screen(Game game) {
		this.game = game;
	}

	@SuppressWarnings("unused")
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		double l = scale*c;

		//Squares
		for(int y = 0; y < game.world.height; y++) {
			for(int x = 0; x < game.world.width; x++) {
				Square s = game.squares[x][y];
				g.drawImage(s.getImage(),
						(int)(Math.round(x*l)) + drawOffsetX, (int)(Math.round(y*l)) + drawOffsetY,
						(int)(Math.round(x*l + l)) + drawOffsetX, (int)(Math.round(y*l + l)) + drawOffsetY,
						0, 0, 24, 24, null);
			}
		}

		//ActionArea
		if(inAction) {
			for (Square p : goArea) {
				g.drawImage(game.greenSquare,
						(int)(Math.round(p.x*l)) + drawOffsetX, (int)(Math.round(p.y*l)) + drawOffsetY,
						(int)(Math.round(p.x*l + l)) + drawOffsetX, (int)(Math.round(p.y*l + l)) + drawOffsetY,
						0, 0, 24, 24, null);
			}
			for (Square p : attackable) {
				g.drawImage(game.redSquare,
						(int)(Math.round(p.x*l)) + drawOffsetX, (int)(Math.round(p.y*l)) + drawOffsetY,
						(int)(Math.round(p.x*l + l)) + drawOffsetX, (int)(Math.round(p.y*l + l)) + drawOffsetY,
						0, 0, 24, 24, null);
			}

			/*XXX*/
			Square p = null;
			Square c = null;
			for(Square n : drawnroad) {
				if (c != null) {
					if (p != null) { //Straights and Edges
						if (p.x != c.x) {
							if (p.x < c.x) {
								if (n.x != c.x) {
									if (n.x < c.x) { //A1
										routeDraw(g, game.moveDeadEnd, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									} else if (n.x > c.x) { //B1
										routeDraw(g, game.moveStraight, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									}
								} else if (n.y != c.y) {
									if (n.y < c.y) { //C1
										routeDraw(g, game.moveEdge, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									} else if (n.y > c.y) { //D1
										routeDraw(g, game.moveEdge, c.x*l, c.y*l + l, c.x*l + l, c.y*l);
									}
								}
							} else if (p.x > c.x) {
								if (n.x != c.x) {
									if (n.x < c.x) { //A2
										routeDraw(g, game.moveStraight, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									} else if (n.x > c.x) { //B2
										routeDraw(g, game.moveDeadEnd, c.x*l + l, c.y*l, c.x*l, c.y*l + l);
									}
								} else if (n.y != c.y) {
									if (n.y < c.y) { //C2
										routeDraw(g, game.moveEdge, c.x*l + l, c.y*l, c.x*l, c.y*l + l);
									} else if (n.y > c.y) { //D2
										routeDraw(g, game.moveEdge, c.x*l + l, c.y*l + l, c.x*l, c.y*l);
									}
								}
							}
						} else if (p.y != c.y) {
							if (p.y < c.y) {
								if (n.x != c.x) {
									if (n.x < c.x) { //A3
										routeDraw(g, game.moveEdge, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									} else if (n.x > c.x) { //B3
										routeDraw(g, game.moveEdge, c.x*l + l, c.y*l, c.x*l, c.y*l + l);
									}
								} else if (n.y != c.y) {
									if (n.y < c.y) { //C3
										routeDraw(g, game.moveDeadEnd2, c.x*l, c.y*l + l, c.x*l + l, c.y*l);
									} else if (n.y > c.y) { //D3
										routeDraw(g, game.moveStraight2, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									}
								}
							} else if (p.y > c.y) {
								if (n.x != c.x) {
									if (n.x < c.x) { //A4
										routeDraw(g, game.moveEdge, c.x*l, c.y*l + l, c.x*l + l, c.y*l);
									} else if (n.x > c.x) { //B4
										routeDraw(g, game.moveEdge, c.x*l + l, c.y*l + l, c.x*l, c.y*l);
									}
								} else if (n.y != c.y) {
									if (n.y < c.y) { //C4
										routeDraw(g, game.moveStraight2, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									} else if (n.y > c.y) { //D4
										routeDraw(g, game.moveDeadEnd2, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
									}
								}
							}
						}
					} else { //Start
						if (n.x != c.x) {
							if (n.x > c.x) {
								routeDraw(g, game.moveDeadEnd, c.x*l + l, c.y*l, c.x*l, c.y*l + l);
							} else if (n.x < c.x) {
								routeDraw(g, game.moveDeadEnd, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
							}
						} else if (n.y != c.y) {
							if (n.y > c.y) {
								routeDraw(g, game.moveDeadEnd2, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
							} else if (n.y < c.y) {
								routeDraw(g, game.moveDeadEnd2, c.x*l, c.y*l + l, c.x*l + l, c.y*l);
							}
						}
					}
				}
				p = c;
				c = n;
			}
			if (p != null && c != null) { //End
				if (p.x < c.x) {
					routeDraw(g, game.moveArrow, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
				} else if (p.x > c.x) {
					routeDraw(g, game.moveArrow, c.x*l + l, c.y*l, c.x*l, c.y*l + l);
				} else if (p.y < c.y) {
					routeDraw(g, game.moveArrow2, c.x*l, c.y*l + l, c.x*l + l, c.y*l);
				} else if (p.y > c.y) {
					routeDraw(g, game.moveArrow2, c.x*l, c.y*l, c.x*l + l, c.y*l + l);
				}
			}
			/*XXX*/
			if(attackable.contains(game.squares[currentSquareX][currentSquareY])) {
				if(selectorImgTick == true) {
					g.drawImage(game.attackMark,
							(int)(Math.round(currentSquareX*l)) + drawOffsetX, (int)(Math.round(currentSquareY*l)) + drawOffsetY,
							(int)(Math.round(currentSquareX*l + l)) + drawOffsetX, (int)(Math.round(currentSquareY*l + l)) + drawOffsetY,
							0, 0, 24, 24, null);
				} else {
					g.drawImage(game.attackMark2,
							(int)(Math.round(currentSquareX*l)) + drawOffsetX, (int)(Math.round(currentSquareY*l)) + drawOffsetY,
							(int)(Math.round(currentSquareX*l + l)) + drawOffsetX, (int)(Math.round(currentSquareY*l + l)) + drawOffsetY,
							0, 0, 24, 24, null);
				}
			}
		}
		if(selectorImgTick == true) {
			g.drawImage(game.currSqrImg,
					(int)(Math.round(currentSquareX*l)) + drawOffsetX, (int)(Math.round(currentSquareY*l)) + drawOffsetY,
					(int)(Math.round(currentSquareX*l + l)) + drawOffsetX, (int)(Math.round(currentSquareY*l + l)) + drawOffsetY,
					0, 0, 24, 24, null);
		} else {
			g.drawImage(game.currSqrImg2,
					(int)(Math.round(currentSquareX*l)) + drawOffsetX, (int)(Math.round(currentSquareY*l)) + drawOffsetY,
					(int)(Math.round(currentSquareX*l + l)) + drawOffsetX, (int)(Math.round(currentSquareY*l + l)) + drawOffsetY,
					0, 0, 24, 24, null);
		}


		//Units
		double m = math1();
		double o = scale*c/15;
		for(Unit u:game.units) {
			if(u.pivot) {
				g.drawImage(u.getImage(),
						(int)(Math.round(u.sX*l + scale + m + u.offX*o)) + drawOffsetX, (int)(Math.round(u.sY*l + m + u.offY*o)) + drawOffsetY,
						(int)(Math.round(u.sX*l + m + u.offX*o)) + drawOffsetX, (int)(Math.round(u.sY*l + scale + m + u.offY*o)) + drawOffsetY,
						0, 0, 16, 16, null);
			} else {
				g.drawImage(u.getImage(),
						(int)(Math.round(u.sX*l + m + u.offX*o)) + drawOffsetX, (int)(Math.round(u.sY*l + m + u.offY*o)) + drawOffsetY,
						(int)(Math.round(u.sX*l + scale + m + u.offX*o)) + drawOffsetX, (int)(Math.round(u.sY*l + scale + m + u.offY*o)) + drawOffsetY,
						0, 0, 16, 16, null);
			}
			BufferedImage h = null;
			switch (u.getHealth()) {
			case 10:
				break;
			case 9: h = game.h9;
			break;
			case 8: h = game.h8;
			break;
			case 7: h = game.h7;
			break;
			case 6: h = game.h6;
			break;
			case 5: h = game.h5;
			break;
			case 4: h = game.h4;
			break;
			case 3: h = game.h3;
			break;
			case 2: h = game.h2;
			break;
			case 1: h = game.h1;
			}
			if(h != null) {
				double dd = l / 2.5;
				double ee = l / 15;
				g.drawImage(h,
						(int)(Math.round(u.sX*l + m + u.offX*o + dd + ee)) + drawOffsetX, (int)(Math.round(u.sY*l + m + u.offY*o + dd + ee)) + drawOffsetY,
						(int)(Math.round(u.sX*l + scale + m + u.offX*o + ee)) + drawOffsetX, (int)(Math.round(u.sY*l + scale + m + u.offY*o + ee)) + drawOffsetY,
						0, 0, 10, 10, null);
			}
		}

	}

	void actionEnd() {
		drawnroad.clear();
		perform = null;
		inAction = false;
		performMovesLeft = 0;
		performMoved = false;
		performAttacked = false;
		goArea.clear();
		attackable.clear();
	}
	
	void endPerformMove() {
		performMoved = true;
		performMovesLeft = 0;
		goArea.clear();
		attackable.clear();
		drawnroad.clear();
	}
	
	void startAction(Unit u) {
		perform = u;
		inAction = true;
		performMovesLeft = perform.speed;
		pathFinder();
		drawnroad.add(game.squares[currentSquareX][currentSquareY]);
	}

	
	//HHHHEEEEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRRRREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
	@Override
	public void actionPerformed(ActionEvent e) {
		PointerInfo a = MouseInfo.getPointerInfo();
		Point b = a.getLocation();
		mouseX = (int) b.getX();
		mouseY = (int) b.getY();
		int x = (int)((mouseX - drawOffsetX - winFSOffX) / (scale * c));
		int y = (int)((mouseY - drawOffsetY - winFSOffY) / (scale * c));
		currentSquareX = x;
		currentSquareY = y;
		if(x >= 0 && x < game.world.width && y >= 0 && y < game.world.height) {
			//System.out.println(x+"-"+y);
			if (lMouseDown) {
				lClickPerformed = true;
			} else if (lClickPerformed) {
				lClickPerformed = false;
				Square sc = game.squares[x][y];
				Square sa = game.squares[drawnArrowX][drawnArrowY];
				Unit u = sc.unitOnSquare;
				
				if(!inAction) {
					if(u != null && !u.moving && !u.attacking) {
						startAction(u);
					}
				} else if(inAction && !performMoved) {
					if(drawnroad.size() > 1 && goArea.contains(sa)) {
						drawnroad.remove();
						if(attackable.contains(sc)) {
							perform.move(drawnArrowX, drawnArrowY, drawnroad, sc);
							performAttacked = true;
							actionEnd();
						}
						else {
							perform.move(drawnArrowX, drawnArrowY, drawnroad, null);
							endPerformMove();
							pathFinder();
							if(attackable.isEmpty()) {
								actionEnd();
							}
						}
					} else if(attackable.contains(sc)) {
						perform.move(drawnArrowX, drawnArrowY, drawnroad, sc);
						performAttacked = true;
						actionEnd();
					}
				} else if(inAction && performMoved) {
					if(attackable.contains(sc)) {
						perform.move(drawnArrowX, drawnArrowY, drawnroad, sc);
						performAttacked = true;
						actionEnd();
					}
				}
			} else if (rMouseDown) {
				rClickPerformed = true;
			} else if (rClickPerformed) {
				rClickPerformed = false;
				actionEnd();
			} else if(inAction) {
				if(lastSquareX != x || lastSquareY != y) {
					if(goArea.contains(game.squares[x][y]) && drawnroad.contains(game.squares[lastSquareX][lastSquareY])) {
						if((lastSquareX + 1 == x && lastSquareY == y|| lastSquareX - 1 == x  && lastSquareY == y||
								lastSquareY + 1 == y  && lastSquareX == x|| lastSquareY - 1 == y && lastSquareX == x) &&
								performMovesLeft > 0) {
							drawnroad.add(game.squares[x][y]);
							drawnArrowX = x;
							drawnArrowY = y;
							performMovesLeft--;
						} else findRoute(x, y);
					} else if(x != drawnArrowX || y != drawnArrowY) findRoute(x, y);
				}
			}
		}
		if (keyUp == true) {
			drawOffsetY = drawOffsetY + 8;
		}
		if (keyDown == true) {
			drawOffsetY = drawOffsetY - 8;
		}
		if (keyLeft == true) {
			drawOffsetX = drawOffsetX + 8;
		}
		if (keyRight == true) {
			drawOffsetX = drawOffsetX - 8;
		}

		if(tickCounter++ > 20) {
			game.gameTick();
			selectorImgTick = !selectorImgTick;
			tickCounter = 0;
		}

		game.quickTick();
		lastSquareX = x;
		lastSquareY = y;


		repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			keyLeft = true;
		}
		if (key == KeyEvent.VK_RIGHT) {
			keyRight = true;
		}
		if (key == KeyEvent.VK_UP) {
			keyUp = true;
		}
		if (key == KeyEvent.VK_DOWN) {
			keyDown = true;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			keyLeft = false;
		}
		if (key == KeyEvent.VK_RIGHT) {
			keyRight = false;
		}
		if (key == KeyEvent.VK_UP) {
			keyUp = false;
		}
		if (key == KeyEvent.VK_DOWN) {
			keyDown = false;
		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == 1) {
			lMouseDown = true;
		} else if(e.getButton() == 3) {
			rMouseDown = true;
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton() == 1) {
			lMouseDown = false;
		} else if(e.getButton() == 3) {
			rMouseDown = false;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	double math1() {
		return (scale/(1/((c-1)/2)));
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int n = e.getWheelRotation();
		n = 2*n;
		scale -= n;
		PointerInfo a = MouseInfo.getPointerInfo();
		Point b = a.getLocation();
		int x = (int) b.getX();
		int y = (int) b.getY();

		drawOffsetX = (int)(x - (x - drawOffsetX) * scale / (scale + n));
		drawOffsetY = (int)(y - (y - drawOffsetY) * scale / (scale + n));

		repaint();

	}

	@Override
	public void componentResized(ComponentEvent e) {
		winWidth = e.getComponent().getWidth();
		winHeight = e.getComponent().getHeight();
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowStateChanged(WindowEvent e) {
		winIsMax = isMaximized(e.getNewState());
		winWasMax = isMaximized(e.getOldState());
		if (winIsMax && !winWasMax) {
			winFSOffX = 0;
			winFSOffY = 24;
		} else if (winWasMax && !winIsMax) {
			winFSOffX = 8;
			winFSOffY = 32;
		}
	}

	void findRoute(int x, int y) {
		if(goArea.contains(game.squares[x][y])) {
			int cx = perform.x, cy = perform.y;
			performMovesLeft = perform.speed;
			drawnroad.clear();
			drawnroad.add(game.squares[cx][cy]);
			while(cx != x || cy != y) {
				if(x < cx) {
					cx--;
					addToRoute(cx, cy);
				}
				else if(x > cx) {
					cx++;
					addToRoute(cx, cy);
				}
				if(y < cy) {
					cy--;
					addToRoute(cx, cy);
				}
				else if(y > cy) {
					cy++;
					addToRoute(cx, cy);
				}
			}
		} else {
			//actionEnd();
		}
	}
	
	void addToRoute(int x, int y) {
		drawnroad.add(game.squares[x][y]);
		drawnArrowX = x;
		drawnArrowY = y;
		performMovesLeft--;
	}

	void pathFinder() {
		pathFinderRecursive(performMovesLeft, perform.x, perform.y);
	}

	void pathFinderRecursive(int movesLeft, int x, int y) {
		Square sr = null, sl = null, sd = null, su = null;
		if(x + 1 < game.world.width) {
			sr = game.squares[x + 1][y];
		}
		if(x > 0) {
			sl = game.squares[x - 1][y];
		}
		if(y + 1 < game.world.height) {
			sd = game.squares[x][y + 1];
		}
		if(y > 0) {
			su = game.squares[x][y - 1];
		}

		if(sr != null && sr.unitOnSquare != null && sr.unitOnSquare != perform) {
			if(!attackable.contains(sr)) {
				attackable.add(sr);
			}
		}
		if(sl != null && sl.unitOnSquare != null && sl.unitOnSquare != perform) {
			if(!attackable.contains(sl)) {
				attackable.add(sl);
			}
		}
		if(sd != null && sd.unitOnSquare != null && sd.unitOnSquare != perform) {
			if(!attackable.contains(sd)) {
				attackable.add(sd);
			}
		}
		if(su != null && su.unitOnSquare != null && su.unitOnSquare != perform) {
			if(!attackable.contains(su)) {
				attackable.add(su);
			}
		}
		if (movesLeft > 0) {

			int nm = movesLeft - 1;


			if(sr != null && sr.unitOnSquare == null && sr.gettingMovedTo == false) {
				if(!goArea.contains(sr)) {
					goArea.add(sr);
				}
				pathFinderRecursive(nm, x + 1, y);
			}
			if(sl != null && sl.unitOnSquare == null && sl.gettingMovedTo == false) {
				if(!goArea.contains(sl)) {
					goArea.add(sl);
				}
				pathFinderRecursive(nm, x - 1, y);
			}
			if(sd != null && sd.unitOnSquare == null && sd.gettingMovedTo == false) {
				if(!goArea.contains(sd)) {
					goArea.add(sd);
				}
				pathFinderRecursive(nm, x, y + 1);
			}
			if(su != null && su.unitOnSquare == null && su.gettingMovedTo == false) {
				if(!goArea.contains(su)) {
					goArea.add(su);
				}
				pathFinderRecursive(nm, x, y - 1);
			}
		}
	}

	void routeDraw(Graphics g, BufferedImage img, double a, double b, double c, double d) {
		g.drawImage(img,
				(int)(Math.round(a)) + drawOffsetX, (int)(Math.round(b)) + drawOffsetY,
				(int)(Math.round(c)) + drawOffsetX, (int)(Math.round(d)) + drawOffsetY,
				0, 0, 24, 24, null);
	}

	private static boolean isMaximized(int state) {
		return (state & Frame.MAXIMIZED_BOTH) == Frame.MAXIMIZED_BOTH;
	}
}
