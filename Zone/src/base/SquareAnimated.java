package base;

import java.awt.image.BufferedImage;

public class SquareAnimated extends Square {
	
	public boolean phase = false;
	BufferedImage image2;
	
	@Override
	void tick() {
		phase = !phase;
	}

	public SquareAnimated(BufferedImage img, BufferedImage img2, int x, int y) {
		super(img, x, y);
		this.image2 = img2;
	}
	
	@Override
	public BufferedImage getImage() {
		if(phase) return this.image2;
		else return this.image;
	}

}
