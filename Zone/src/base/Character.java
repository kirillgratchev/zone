package base;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Character extends Entity{

	protected int x, y;
	protected boolean clicked = false;
	
	protected Character(BufferedImage img, int x, int y) {
		super(img, x, y);
		this.x = x;
		this.y = y;
		
		
	}
	
	void clicked() {
		clicked = true;
	}
	
	void endAction() {
		clicked = false;
	}
}
