package base;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Entity {

	protected BufferedImage image;
	
	protected int x, y;

	protected Entity(BufferedImage img, int x, int y){
		image = img;
		this.x = x;
		this.y = y;
	}
	
	public static BufferedImage loadImg(String img) {
		try {
			BufferedImage image = ImageIO.read(Entity.class.getResourceAsStream(img));
			for(int x = 0; x < image.getWidth(); ++x) {
				for(int y = 0; y < image.getHeight(); ++y) {
					int c = image.getRGB(x, y);
					int a = (c&0xFF000000)>>24;
					int r = (c&0x00FF0000)>>16;
					int g = (c&0x0000FF00)>>8;
					int b = c&0x000000FF;
					r = r * (255 / 255);
					g = g * (255 / 255);
					b = b * (255 / 255);
					image.setRGB(x, y, (a<<24) | (r<<16) | (g<<8) | (b));
				}
			}
			return image;
		} catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public BufferedImage getImage() {
		return this.image;
	}
}
