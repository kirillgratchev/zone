package base;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class Game {

	protected World world;
	protected List<Unit> units = new LinkedList<>();
	protected Square[][] squares;

	BufferedImage plains = Entity.loadImg("plains.png");
	BufferedImage water = Entity.loadImg("water.png");
	BufferedImage water2 = Entity.loadImg("water2.png");

	BufferedImage btank = Entity.loadImg("battle_tank.png");
	BufferedImage btankdrv = Entity.loadImg("battle_tank_drv.png");
	BufferedImage htank = Entity.loadImg("hunter_tank.png");
	BufferedImage htankdrv = Entity.loadImg("hunter_tank_drv.png");
	BufferedImage humvy = Entity.loadImg("car.png");
	BufferedImage humvydrv = Entity.loadImg("car_drv.png");

	BufferedImage currSqrImg = Entity.loadImg("selector.png");
	BufferedImage currSqrImg2 = Entity.loadImg("selector2.png");
	BufferedImage attackMark = Entity.loadImg("selector_arrows.png");
	BufferedImage attackMark2 = Entity.loadImg("selector_arrows2.png");
	
	BufferedImage greenSquare = Entity.loadImg("s_green.png");
	BufferedImage redSquare = Entity.loadImg("s_red.png");
	BufferedImage orangeSquare = Entity.loadImg("s_orange.png");

	BufferedImage moveArrow = Entity.loadImg("move_arrow.png");
	BufferedImage moveArrow2 = Entity.loadImg("move_arrow_2.png");
	BufferedImage moveDeadEnd = Entity.loadImg("move_deadend.png");
	BufferedImage moveDeadEnd2 = Entity.loadImg("move_deadend_2.png");
	BufferedImage moveEdge = Entity.loadImg("move_edge.png");
	BufferedImage moveStraight = Entity.loadImg("move_straight.png");
	BufferedImage moveStraight2 = Entity.loadImg("move_straight_2.png");
	
	BufferedImage h1 = Entity.loadImg("h_1.png");
	BufferedImage h2 = Entity.loadImg("h_2.png");
	BufferedImage h3 = Entity.loadImg("h_3.png");
	BufferedImage h4 = Entity.loadImg("h_4.png");
	BufferedImage h5 = Entity.loadImg("h_5.png");
	BufferedImage h6 = Entity.loadImg("h_6.png");
	BufferedImage h7 = Entity.loadImg("h_7.png");
	BufferedImage h8 = Entity.loadImg("h_8.png");
	BufferedImage h9 = Entity.loadImg("h_9.png");

	Game(World world){

		this.world = world;

		squares = new Square[world.width][world.height];
		for (int i = 0; i < world.width; i++) {
			for (int j = 0; j < world.height; j++) {

				Square s;
				if (i < 10 || j < 5) {
					s = new Square(plains, i, j);
				} else {
					s = new SquareAnimated(water, water2, i, j);
				}
				squares[i][j] = s;
			}
		}

		newHunterTank(2, 3);
		newBattleTank(5, 5);
		newHumvy(5, 8);
		newHumvy(6, 6);



	}

	void gameTick() {
		for (int i = 0; i < world.width; i++) {
			for (int j = 0; j < world.height; j++) {
				Square s = squares[i][j];
				s.tick();
			}
		}
		for (Unit u : units) {
			u.tick();
		}

	}

	void quickTick() {
		
		for (Unit u : units) {
			u.quicktick();
		}
	}

	void moveUnit(Unit u, int x, int y) {
		System.out.println("Moving Unit "+u+" from "+u.x+"-"+u.y+" to "+x+"-"+y);
		squares[u.x][u.y].unitOnSquare = null;
		squares[x][y].unitOnSquare = u;
		u.x = x;
		u.y = y;

	}
	
	void terminateUnit(Unit u) {
		System.out.println("Terminated Unit "+u);
		squares[u.x][u.y].unitOnSquare = null;
		units.remove(u);
	}

	void newHunterTank(int x, int y) {
		Unit t = new Vehicle(htank, htankdrv, x, y, this, 4, 50, 60);
		units.add(t);
		squares[x][y].unitOnSquare = t;
	}
	void newBattleTank(int x, int y) {
		Unit t = new Vehicle(btank, btankdrv, x, y, this, 3, 60, 70);
		units.add(t);
		squares[x][y].unitOnSquare = t;
	}
	void newHumvy(int x, int y) {
		Unit t = new Vehicle(humvy, humvydrv, x, y, this, 6, 30, 40);
		units.add(t);
		squares[x][y].unitOnSquare = t;
	}

}
